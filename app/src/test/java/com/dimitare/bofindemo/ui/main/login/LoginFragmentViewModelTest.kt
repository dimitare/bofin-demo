package com.dimitare.bofindemo.ui.main.login

import android.content.SharedPreferences
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.dimitare.bofindemo.BuildConfig
import com.dimitare.bofindemo.data.helper.SharedPreferencesHelper
import com.dimitare.bofindemo.data.helper.UserHelper
import com.dimitare.bofindemo.data.model.LoggedInUser
import com.dimitare.bofindemo.data.model.Token
import com.dimitare.bofindemo.data.model.VisibleStatus
import com.dimitare.bofindemo.data.services.FakeDemoRepository
import com.dimitare.bofindemo.data.services.TestSchedulersProvider
import com.dimitare.bofindemo.domain.GetLoginUseCase
import com.f2prateek.rx.preferences2.RxSharedPreferences
import io.reactivex.Flowable
import io.reactivex.schedulers.TestScheduler
import junit.framework.Assert.assertNotNull
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.Mockito.*

class LoginFragmentViewModelTest {

    private lateinit var mLoginFragmentViewModel: LoginFragmentViewModel

    // Use a fake repository to be injected into the viewmodel
    @Mock
    private lateinit var mRepository: FakeDemoRepository
    private val mTestScheduler = TestScheduler()

    @Mock
    private lateinit var mGetLoginUseCase: GetLoginUseCase

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    val token = Token(token = "5DE6E0C53BB8BB6CBB392526FA278D47")
    @Mock
    var mObserverToken: Observer<Token>? = null

    @Mock
    var mObserverVisibleStatus: Observer<VisibleStatus>? = null

    @Mock
    var mObserverLoginResult: Observer<LoginResult>? = null

    @Mock
    var mUserHelper: UserHelper? = null

    @Before
    fun setupViewModel() {
        MockitoAnnotations.initMocks(this)
        mRepository = mock(
            FakeDemoRepository::class.java
        )

        mGetLoginUseCase = GetLoginUseCase(mRepository)
        mLoginFragmentViewModel = LoginFragmentViewModel(
            mGetLoginUseCase,
            TestSchedulersProvider(mTestScheduler)
        )
        val sharedPreferences = mock(SharedPreferences::class.java)
        val sharedPreferencesHelper = SharedPreferencesHelper(RxSharedPreferences.create(sharedPreferences))
        mUserHelper = UserHelper(sharedPreferencesHelper)
        mLoginFragmentViewModel.userHelper = mUserHelper!!
        mGetLoginUseCase.userHelper = mUserHelper!!
        reset(mObserverToken)
        mObserverVisibleStatus?.let { mLoginFragmentViewModel.mLoading.observeForever(it) }
        mObserverLoginResult?.let { mLoginFragmentViewModel.loginResult.observeForever(it) }
    }

    @Test
    fun testNull() {
        `when`(mGetLoginUseCase.login(BuildConfig.BOFIN_USERNAME, BuildConfig.BOFIN_PASSWORD)).thenReturn(Flowable.just(LoggedInUser(BuildConfig.BOFIN_USERNAME, token = token.token))).thenReturn(null)
        assertNotNull(mLoginFragmentViewModel.mLoading)
        assertTrue(mLoginFragmentViewModel.mLoading.hasObservers())
    }

    @Test
    fun verifyVisibility() {
        `when`(mGetLoginUseCase.login(BuildConfig.BOFIN_USERNAME, BuildConfig.BOFIN_PASSWORD)).thenReturn(Flowable.just(LoggedInUser(BuildConfig.BOFIN_USERNAME, token = token.token)))
        assertNotNull(mLoginFragmentViewModel.mLoading)
        mLoginFragmentViewModel.startViewModel()
        mTestScheduler.triggerActions()
        verify(mObserverVisibleStatus)?.onChanged(VisibleStatus.GONE)
    }
}