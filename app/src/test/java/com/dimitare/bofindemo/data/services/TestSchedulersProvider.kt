package com.dimitare.bofindemo.data.services

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler

class TestSchedulersProvider(val mScheduler: TestScheduler = TestScheduler()) : SchedulerProvider {
    override fun ui(): Scheduler {
        return mScheduler
    }

    override fun computation(): Scheduler {
        return Schedulers.computation()
    }

    override fun io(): Scheduler {
        return mScheduler
    }
}