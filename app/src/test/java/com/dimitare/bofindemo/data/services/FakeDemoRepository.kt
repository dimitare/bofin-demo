package com.dimitare.bofindemo.data.services

import com.dimitare.bofindemo.data.model.LoggedInUser
import com.dimitare.bofindemo.data.model.User
import io.reactivex.Flowable
import io.reactivex.Single

open class FakeDemoRepository : DemoRepository {
    override fun login(user: String, password: String): Flowable<LoggedInUser> {
        return Flowable.empty()
    }

    override fun getUsers(page: Int, token: String, filerValue: String): Single<MutableList<User>> {
        TODO("Not yet implemented")
    }

    override fun getUserDetail(userId: Int): Single<User> {
        TODO("Not yet implemented")
    }
}