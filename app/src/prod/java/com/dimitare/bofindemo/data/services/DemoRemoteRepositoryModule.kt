package com.dimitare.bofindemo.data.services

import com.dimitare.bofindemo.data.services.remote.DemoRemoteDataSource
import com.dimitare.bofindemo.data.services.remote.DemoServiceApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class DemoRemoteRepositoryModule {
    @Provides
    @Singleton
    fun providesService(
        dataSource: DemoDataSource
    ): DemoRepository {
        return DemoRepositoryImpl(dataSource)
    }

    @Provides
    fun provideDemoDataSource(demoService: DemoServiceApi): DemoDataSource {
        return DemoRemoteDataSource(demoService)
    }

    @Provides
    @Singleton
    fun bindSchedulerProvider(): SchedulerProvider{
        return ApplicationSchedulerProvider()
    }

    @Provides
    fun providesDemoService(retrofit: Retrofit): DemoServiceApi {
        return retrofit.create(DemoServiceApi::class.java)
    }
}