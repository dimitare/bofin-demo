package com.dimitare.bofindemo.data.services

import android.app.Application
import com.dimitare.bofindemo.data.services.remote.DemoRemoteDataSource
import com.dimitare.bofindemo.data.services.remote.DemoServiceApi
import com.dimitare.bofindemo.data.services.remote.DemoServiceMock
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.mock.BehaviorDelegate
import javax.inject.Named
import javax.inject.Singleton

@Module
class DemoRemoteRepositoryModule {
    @Provides
    @Singleton
    fun providesService(
        dataSource: DemoDataSource
    ): DemoRepository {
        return DemoRepositoryImpl(dataSource)
    }

    @Provides
    fun provideDemoDataSource(@Named("mockServiceApi") demoService: DemoServiceApi): DemoDataSource {
        return DemoRemoteDataSource(demoService)
    }

    @Provides
    @Singleton
    fun bindSchedulerProvider(): SchedulerProvider{
        return ApplicationSchedulerProvider()
    }

    @Provides
    @Named("serviceApi")
    fun providesDemoService(retrofit: Retrofit): DemoServiceApi {
        return retrofit.create(DemoServiceApi::class.java)
    }

    @Provides
    @Named("mockServiceApi")
    fun provideMockDemoService(context: Application, delegate: BehaviorDelegate<DemoServiceApi>): DemoServiceApi {
        return DemoServiceMock(context, delegate)
    }
}