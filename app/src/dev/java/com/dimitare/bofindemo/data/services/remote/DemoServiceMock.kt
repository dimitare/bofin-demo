package com.dimitare.bofindemo.data.services.remote

import android.app.Application
import com.dimitare.bofindemo.BuildConfig
import com.dimitare.bofindemo.data.model.Token
import com.dimitare.bofindemo.data.model.User
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.gson.GsonBuilder
import io.reactivex.Flowable
import retrofit2.mock.BehaviorDelegate
import java.nio.charset.Charset

class DemoServiceMock internal constructor(
    val context: Application,
    val delegate: BehaviorDelegate<DemoServiceApi>
) : DemoServiceApi {
    override fun login(loginRequest: LoginRequest): Flowable<Token> {
        if( loginRequest.user.equals(BuildConfig.BOFIN_USERNAME, true)){
            val mapper = jacksonObjectMapper()
            val token = mapper.readValue(loginSuccess, Token::class.java)
            return delegate.returningResponse(token).login(loginRequest)
        }
        throw IllegalArgumentException(loginFailed)
    }

    private val loginSuccess = "{\n" +
            "    \"token\": \"5DE6E0C53BB8BB6CBB392526FA278D47\"\n" +
            "}"

    private val loginFailed = "invalid user and password"

    override fun getUsers(authorization: String): Flowable<List<User>> {
        val mapper = jacksonObjectMapper()
        val data = provideUserDataRawData()
        val listType: TypeReference<List<User?>?> =
            object : TypeReference<List<User?>?>() {}
        val user = mapper.readValue(data, listType)
        return delegate.returningResponse(user).getUsers(authorization)
    }

    private fun provideUserDataRawData(): String {
        val inputStream = context.assets.open("user_data.json")
        val size = inputStream.available()
        val buffer = ByteArray(size)
        inputStream.read(buffer)
        inputStream.close()
        return buffer.toString(Charset.defaultCharset())
    }
}