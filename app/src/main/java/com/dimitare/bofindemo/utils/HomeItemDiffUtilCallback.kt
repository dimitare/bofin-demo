package com.dimitare.bofindemo.utils

import androidx.recyclerview.widget.DiffUtil
import com.dimitare.bofindemo.data.model.User

class HomeItemDiffUtilCallback(
    newList: List<User>,
    oldList: List<User>
) :
    DiffUtil.Callback() {
    private val oldItems: List<User>
    private val newItems: List<User>
    override fun getOldListSize(): Int {
        return oldItems.size
    }

    override fun getNewListSize(): Int {
        return newItems.size
    }

    override fun areItemsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        return oldItems[oldItemPosition].id == newItems[newItemPosition].id
    }

    override fun areContentsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        return oldItems[oldItemPosition].id == newItems[newItemPosition].id
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }

    init {
        newItems = newList
        oldItems = oldList
    }
}