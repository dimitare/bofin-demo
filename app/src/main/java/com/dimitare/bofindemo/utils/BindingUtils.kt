package com.dimitare.bofindemo.utils

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dimitare.bofindemo.data.model.User
import com.dimitare.bofindemo.data.model.VisibleStatus
import com.dimitare.bofindemo.ui.main.home.HomeAdapter

@BindingAdapter("usersData")
fun <T> setRecyclerViewProperties(
    recyclerView: RecyclerView,
    items: Collection<User>?
) {
    items?.let {
        (recyclerView.adapter as? HomeAdapter)?.setData(items)
        recyclerView.adapter?.notifyDataSetChanged()
    }
}

@BindingAdapter("app:url")
fun updateUrl(view: ImageView, url: String?) {
    Glide.with(view).load(url).into(view)
}

@BindingAdapter("app:error_message", "app:visibility")
fun updateErrorState(view: TextView, error_message: String?, visibility: VisibleStatus) {
    view.text = error_message
}