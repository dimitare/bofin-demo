package com.dimitare.bofindemo.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dimitare.bofindemo.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
