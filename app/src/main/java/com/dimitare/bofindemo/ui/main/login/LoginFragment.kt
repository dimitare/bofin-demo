package com.dimitare.bofindemo.ui.main.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dimitare.bofindemo.R
import com.dimitare.bofindemo.data.helper.SharedPreferencesHelper
import com.dimitare.bofindemo.databinding.LoginFragmentViewBinding
import com.dimitare.bofindemo.ext.USER_VALIDATOR
import com.dimitare.bofindemo.ext.PASSWORD_VALIDATOR
import com.dimitare.bofindemo.ext.validate
import com.dimitare.bofindemo.ui.base.fragment.BaseDaggerFragment
import com.jakewharton.rxbinding4.view.clicks
import com.jakewharton.rxbinding4.widget.textChanges
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.Observables
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class LoginFragment: BaseDaggerFragment<LoginFragmentViewModel>(), LoginFlow {
    override val mViewModel by viewModels<LoginFragmentViewModel>()

    @Inject
    lateinit var mSharedPreferencesHelper: SharedPreferencesHelper
    private lateinit var mDataBinding: LoginFragmentViewBinding
    protected var destroyViewDisposable: CompositeDisposable? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBinding = LoginFragmentViewBinding.inflate(inflater, container, false).apply {
            viewModel = mViewModel
        }
        mDataBinding.lifecycleOwner = this
        activity?.title = getString(R.string.app_name)
        return mDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        destroyViewDisposable = CompositeDisposable()

        destroyViewDisposable?.add(
            Observables
                .combineLatest(
                    mDataBinding.user.textChanges()
                        .debounce(500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map { t ->
                            val emailValid = t.validate(USER_VALIDATOR)
//                            binding.signInEmailLayout.error =
//                                if (t.isNotEmpty() && !emailValid) "Invalid email" else null
                            emailValid
                        },
                    mDataBinding.password.textChanges()
                        .debounce(500, TimeUnit.MILLISECONDS)
                        .observeOn(AndroidSchedulers.mainThread())
                        .map { t ->
                            val passwordValid = t.validate(PASSWORD_VALIDATOR)
//                            binding.signInPasswordLayout.error =
//                                if (t.isNotEmpty() && !passwordValid) "Invalid password (minimum 8 chars)" else null
                            passwordValid
                        }
                ) { t1, t2 -> t1 && t2 }
                .observeOn(AndroidSchedulers.mainThread())
                .startWithItem(false)
                .subscribe { mDataBinding.login.isEnabled = it })

        mDataBinding.login.setOnClickListener {
            mDataBinding.viewModel?.login(mDataBinding.user.text.toString(), mDataBinding.password.text.toString(), mDataBinding.gender.isChecked)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        destroyViewDisposable?.dispose()
    }
}