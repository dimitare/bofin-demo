package com.dimitare.bofindemo.ui.main.home

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import com.dimitare.bofindemo.R
import com.dimitare.bofindemo.di.scope.FragmentScope
import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewFragmentViewModel
import com.dimitare.bofindemo.data.model.User
import com.dimitare.bofindemo.data.model.VisibleStatus
import com.dimitare.bofindemo.data.services.SchedulerProvider
import com.dimitare.bofindemo.domain.GetUsersListUseCase
import com.dimitare.bofindemo.ui.common.navigation.NavigationRequest
import javax.inject.Inject

@FragmentScope
class HomeFragmentViewModel @Inject constructor(
    val mHomeListUseCase: GetUsersListUseCase,
    private val mSchedulerProvider: SchedulerProvider
) : BaseViewFragmentViewModel() {

    override fun handleArguments(argument: Bundle) {
        super.handleArguments(argument)
    }

    private var mPage = 1;
    private val _loading = MutableLiveData<VisibleStatus>()
    val mLoading = _loading

    private val _users = MutableLiveData<MutableList<User>>(mutableListOf())
    val mUsers = _users

    private val _errorMessage = MutableLiveData<String>()
    val mErrorMessage = _errorMessage

    override fun startViewModel() {
        super.startViewModel()
        if (_users.value != null && _users.value?.size == 0) {
            getUsers()
        }
    }

    @SuppressLint("CheckResult")
    internal fun getUsers() {
        addDisposable(
            mHomeListUseCase(mPage, userHelper.token!!, userHelper.getGenderSelection().name)
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .doOnSubscribe {
                    _loading.value = VisibleStatus.VISIBLE
                    _users.value = mutableListOf()
                }
                .subscribe({ result ->
                    onSuccess(result.toMutableList())
                }, { t ->
                    onError(t)
                })
        )
    }

    override fun handleUserDisconnected() {
        super.handleUserDisconnected()
        navigate(NavigationRequest.Push(R.id.action_transition_to_login))
    }

    private fun onSuccess(list: MutableList<User>) {
        _users.postValue(list)
        _errorMessage.value = null
        _loading.postValue(VisibleStatus.GONE)
    }

    private fun onError(error: Throwable) {
        _errorMessage.value = error.localizedMessage
        _loading.postValue(VisibleStatus.GONE)

    }

    fun logout() {
        userHelper.disconnect()
    }
}