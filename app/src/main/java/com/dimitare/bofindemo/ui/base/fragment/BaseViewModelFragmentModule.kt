package com.dimitare.bofindemo.ui.base.fragment

import androidx.lifecycle.ViewModelProvider
import com.dimitare.bofindemo.di.qualifier.FragmentContext
import com.dimitare.bofindemo.di.scope.FragmentScope
import com.dimitare.bofindemo.ui.common.viewmodel.factory.FragmentViewModelFactory
import dagger.Binds
import dagger.Module

@Module
abstract class BaseViewModelFragmentModule {
    @Binds
    @FragmentScope
    @FragmentContext
    abstract fun bindViewModelFactory(viewModelFactory: FragmentViewModelFactory): ViewModelProvider.Factory
}