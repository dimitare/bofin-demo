package com.dimitare.bofindemo.ui.base.viewmodel

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.LiveData
import com.dimitare.bofindemo.data.helper.UserHelper
import com.dimitare.bofindemo.ext.getClassTag
import com.dimitare.bofindemo.ui.common.navigation.NavigationRequest
import com.dimitare.bofindemo.ui.common.viewmodel.NavigationEvent
import io.reactivex.rxjava3.core.BackpressureStrategy
import javax.inject.Inject

abstract class BaseViewNavigationViewModel : BaseViewModel() {
    private val savable = Bundle()
    private val _navigation: NavigationEvent = NavigationEvent()

    @Inject
    lateinit var userHelper: UserHelper
    val navigation: LiveData<NavigationRequest> = _navigation

    open fun handleIntent(intent: Intent) {}

    open fun handleCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            savable.putAll(savedInstanceState.getBundle(getClassTag()))
        }

        disposables.add(userHelper.statusObservable.toFlowable(BackpressureStrategy.LATEST)
            .subscribe {
                if (it) handleUserConnected()
                else handleUserDisconnected()
            })
    }

    open fun handleUserConnected() {}

    open fun handleUserDisconnected() {}

    fun navigate(navigationRequest: NavigationRequest) {
        _navigation.value = navigationRequest
    }
}