package com.dimitare.bofindemo.ui.main.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.dimitare.bofindemo.databinding.DetailsFragmentViewBinding
import com.dimitare.bofindemo.ui.base.fragment.BaseDaggerFragment

class DetailsFragment : BaseDaggerFragment<DetailsFragmentViewModel>() {
    override val mViewModel by viewModels<DetailsFragmentViewModel>()
    private lateinit var mDataBinding: DetailsFragmentViewBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBinding = DetailsFragmentViewBinding.inflate(inflater, container, false).apply {
            viewModel = mViewModel
        }
        mDataBinding.lifecycleOwner = this
        return mDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args: DetailsFragmentArgs by navArgs()
        mDataBinding.viewModel?.fetchUserDetail(args.userId)
    }
}