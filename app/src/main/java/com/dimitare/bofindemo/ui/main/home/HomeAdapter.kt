package com.dimitare.bofindemo.ui.main.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.dimitare.bofindemo.data.model.User
import com.dimitare.bofindemo.databinding.HomeFragmentViewItemBinding
import com.dimitare.bofindemo.utils.HomeItemDiffUtilCallback

class HomeAdapter(private val onUserItemClickCallback: ((User) -> Unit)?): RecyclerView.Adapter<HomeAdapter.HomeViewHolder>(){
    var mItems: MutableList<User> = mutableListOf()

    fun setData(items: Collection<User>) {
        val oldList: List<User> = mItems
        mItems = items as MutableList<User>
        val diffResult =
            DiffUtil.calculateDiff(HomeItemDiffUtilCallback(items, oldList), true)
        diffResult.dispatchUpdatesTo(this)
    }

    inner class HomeViewHolder(val binding: HomeFragmentViewItemBinding) :
        RecyclerView.ViewHolder(binding.root){
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val binding =
            HomeFragmentViewItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return HomeViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        val userItem = mItems[position]
        holder.binding.userItem = userItem
        holder.binding.root.setOnClickListener {
            val item = mItems[position]
            onUserItemClickCallback?.invoke(item)
        }
        holder.binding.executePendingBindings()
    }
}