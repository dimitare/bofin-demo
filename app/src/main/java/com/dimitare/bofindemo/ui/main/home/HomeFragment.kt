package com.dimitare.bofindemo.ui.main.home

import android.os.Bundle
import android.view.*
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.dimitare.bofindemo.R
import com.dimitare.bofindemo.databinding.HomeFragmentViewBinding
import com.dimitare.bofindemo.ui.base.fragment.BaseDaggerFragment

class HomeFragment : BaseDaggerFragment<HomeFragmentViewModel>() {
    override val mViewModel by viewModels<HomeFragmentViewModel>()
    private lateinit var mDataBinding: HomeFragmentViewBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true);
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mDataBinding = HomeFragmentViewBinding.inflate(inflater, container, false).apply {
            viewModel = mViewModel
        }
        mDataBinding.lifecycleOwner = this
        activity?.title = getString(R.string.app_name)
        return mDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDataBinding.recyclerView.apply {
            this@HomeFragment.activity?.let {
                adapter =
                    HomeAdapter { userItem ->
                        val direction = HomeFragmentDirections.actionHomeFragmentToDetailFragment(userItem.id)
                        findNavController().navigate(direction)
                    }
            }
        }
    }

    override fun onStart() {
        super.onStart()
        mDataBinding.viewModel?.start()
    }

    override fun onStop() {
        super.onStop()
        mDataBinding.viewModel?.stop()
    }
}