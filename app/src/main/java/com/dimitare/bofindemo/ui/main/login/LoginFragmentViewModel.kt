package com.dimitare.bofindemo.ui.main.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.dimitare.bofindemo.data.model.LoginError
import com.dimitare.bofindemo.data.model.VisibleStatus
import com.dimitare.bofindemo.data.services.SchedulerProvider
import com.dimitare.bofindemo.domain.GetLoginUseCase
import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewFragmentViewModel
import com.dimitare.bofindemo.ui.common.Gender
import com.dimitare.bofindemo.ui.common.navigation.NavigationRequest
import com.google.gson.GsonBuilder
import retrofit2.HttpException
import javax.inject.Inject

class LoginFragmentViewModel @Inject constructor(
    private val mLoginUseCase: GetLoginUseCase,
    private val mSchedulerProvider: SchedulerProvider
) : BaseViewFragmentViewModel() {

    private val _loading = MutableLiveData<VisibleStatus>()
    val mLoading = _loading

    init {
        _loading.value = VisibleStatus.GONE
    }

    private val _loginResult = MutableLiveData<LoginResult>()
    val loginResult: LiveData<LoginResult> = _loginResult

    fun login(username: String, password: String, checked: Boolean) {
        addDisposable(mLoginUseCase.updateGender(convertToGender(checked))
            .subscribeOn(mSchedulerProvider.io())
            .flatMap {
                mLoginUseCase.login(username, password)
            }
            .flatMap {
                mLoginUseCase.updateUserData(it)
            }
            .observeOn(mSchedulerProvider.ui())
            .doOnSubscribe {
                _loading.value = VisibleStatus.VISIBLE
            }
            .subscribe({
                _loginResult.value = LoginResult(success = it, error = 0)
                _loading.value = VisibleStatus.GONE
                navigate(NavigationRequest.Pop())
            }, {
                when(it){
                    is HttpException -> {
                        val gson = GsonBuilder().setPrettyPrinting().create()
                        val errorMessage = gson.fromJson(it.response()?.errorBody()?.string(), LoginError::class.java)
                        _loginResult.value = LoginResult(error = 1, errorMessage = errorMessage.error)
                    }
                    else -> _loginResult.value = LoginResult(error = 1, errorMessage = it.localizedMessage)
                }
                _loading.value = VisibleStatus.GONE
            })
        )
    }

    private fun convertToGender(checked: Boolean): Int {
        return if (checked) Gender.Female.ordinal else Gender.Male.ordinal
    }
}