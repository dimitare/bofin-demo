package com.dimitare.bofindemo.ui.common.viewmodel.factory

import androidx.lifecycle.ViewModel
import com.dimitare.bofindemo.di.scope.FragmentScope
import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewFragmentViewModel
import com.dimitare.bofindemo.ui.base.viewmodel.factory.BaseViewModelFactory
import javax.inject.Inject
import javax.inject.Provider

@FragmentScope
class FragmentViewModelFactory @Inject constructor(
    creators: Map<Class<out BaseViewFragmentViewModel>,
            @JvmSuppressWildcards Provider<BaseViewFragmentViewModel>>
) : BaseViewModelFactory(creators as Map<Class<out ViewModel>, Provider<ViewModel>>)