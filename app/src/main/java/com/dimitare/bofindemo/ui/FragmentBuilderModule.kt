package com.dimitare.bofindemo.ui

import com.dimitare.bofindemo.di.scope.FragmentScope
import com.dimitare.bofindemo.ui.main.details.DetailsFragment
import com.dimitare.bofindemo.ui.main.details.DetailsFragmentModule
import com.dimitare.bofindemo.ui.main.home.HomeFragment
import com.dimitare.bofindemo.ui.main.home.HomeFragmentModule
import com.dimitare.bofindemo.ui.main.login.LoginFragment
import com.dimitare.bofindemo.ui.main.login.LoginFragmentModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuilderModule {
    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    @FragmentScope
    abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector(modules = [DetailsFragmentModule::class])
    @FragmentScope
    abstract fun contributeDetailsFragment(): DetailsFragment

    @ContributesAndroidInjector(modules = [LoginFragmentModule::class])
    @FragmentScope
    abstract fun contributeLoginFragment(): LoginFragment
}