package com.dimitare.bofindemo.ui.base.viewmodel

import android.os.Bundle
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewFragmentViewModel: BaseViewNavigationViewModel(){
    open fun handleArguments(argument: Bundle) {}

    private val mCompositeDisposable = CompositeDisposable()
    fun start() {
        startViewModel()
    }

    fun stop() {
        stopViewModel()
    }

    internal open fun startViewModel() {
        userHelper.updateLogin()
    }

    internal open fun stopViewModel() {
        onCleared()
    }

    fun addDisposable(disposable: Disposable) {
        mCompositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        mCompositeDisposable.clear()
    }
}