package com.dimitare.bofindemo.ui.main.login

data class LoginResult(
    val success: String? = null,
    val error: Int? = null,
    val errorMessage: String? = null
)