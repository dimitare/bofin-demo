package com.dimitare.bofindemo.ui.common

import com.fasterxml.jackson.annotation.JsonFormat

//sealed class Gender(val value: Int, val gender: String) {
//    object Male : Gender(value = 0, gender = "Male")
//    object Female : Gender(value = 1, gender = "Female")
//}

@JsonFormat(shape = JsonFormat.Shape.STRING)
enum class Gender {
    Male,
    Female;

    companion object {
        fun nullableValueOf(name: String?) = when (name) {
            null -> null
            else -> valueOf(name)
        }
    }
}