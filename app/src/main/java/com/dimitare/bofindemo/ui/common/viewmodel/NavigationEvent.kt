package com.dimitare.bofindemo.ui.common.viewmodel

import com.dimitare.bofindemo.ui.common.navigation.NavigationRequest

class NavigationEvent : SingleLiveEvent<NavigationRequest>()