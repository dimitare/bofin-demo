package com.dimitare.bofindemo.ui.base.fragment

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.dimitare.bofindemo.di.qualifier.FragmentContext
import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewFragmentViewModel
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject
import androidx.fragment.app.viewModels as viewModelsInternal
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.dimitare.bofindemo.ext.navigate

abstract class BaseDaggerFragment<VM : BaseViewFragmentViewModel> : Fragment(), HasAndroidInjector {
    @Inject
    protected lateinit var androidInjector: DispatchingAndroidInjector<Any>
    protected val navController: NavController
        get() = findNavController()

    @Inject
    @FragmentContext
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    abstract val mViewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mViewModel.handleCreate(savedInstanceState)
        activity?.intent?.let { mViewModel.handleIntent(it) }
        arguments?.let { mViewModel.handleArguments(it) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.navigation.observe(
            viewLifecycleOwner,
            Observer { request ->
                request?.let {
                    navController.navigate(it)
                }
            })
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    @Suppress("UNUSED_PARAMETER")
    protected inline fun <reified VM : ViewModel> viewModels(
        ownerProducer: () -> ViewModelStoreOwner = { this }
    ) = viewModelsInternal<VM> { viewModelFactory }
}
