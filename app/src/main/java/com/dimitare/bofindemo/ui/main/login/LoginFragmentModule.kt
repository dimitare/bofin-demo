package com.dimitare.bofindemo.ui.main.login

import androidx.fragment.app.Fragment
import com.dimitare.bofindemo.di.keys.FragmentViewModelKey
import com.dimitare.bofindemo.di.scope.FragmentScope
import com.dimitare.bofindemo.ui.base.fragment.BaseViewModelFragmentModule
import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewFragmentViewModel
import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [BaseViewModelFragmentModule::class])
abstract class LoginFragmentModule: BaseViewModel() {
    @Binds
    @FragmentScope
    abstract fun bindFragment(fragment: LoginFragment): Fragment

    @Binds
    @IntoMap
    @FragmentViewModelKey(LoginFragmentViewModel::class)
    @FragmentScope
    abstract fun bindViewModel(viewModel: LoginFragmentViewModel): BaseViewFragmentViewModel
}