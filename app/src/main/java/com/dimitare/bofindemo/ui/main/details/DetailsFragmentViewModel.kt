package com.dimitare.bofindemo.ui.main.details

import androidx.lifecycle.MutableLiveData
import com.dimitare.bofindemo.data.model.User
import com.dimitare.bofindemo.data.model.VisibleStatus
import com.dimitare.bofindemo.data.services.SchedulerProvider
import com.dimitare.bofindemo.domain.GetUserDetailsUseCase
import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewFragmentViewModel
import javax.inject.Inject

class DetailsFragmentViewModel @Inject constructor(
    val mGetUserDetailsUseCase: GetUserDetailsUseCase,
    private val mSchedulerProvider: SchedulerProvider
) : BaseViewFragmentViewModel() {
    private val _loading = MutableLiveData<VisibleStatus>()
    val mLoading = _loading

    private val _user = MutableLiveData<User>()
    val mUser = _user

    fun fetchUserDetail(userId: Int) {
        mGetUserDetailsUseCase(userId)
            .subscribeOn(mSchedulerProvider.io())
            .observeOn(mSchedulerProvider.ui())
            .doOnSubscribe {
                _loading.value = VisibleStatus.VISIBLE
            }
            .subscribe({
                _user.value = it
                _loading.value = VisibleStatus.GONE
            }, {
                _user.value = null
                _loading.value = VisibleStatus.GONE
            })
    }
}