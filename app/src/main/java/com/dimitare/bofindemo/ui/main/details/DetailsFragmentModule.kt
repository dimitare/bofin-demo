package com.dimitare.bofindemo.ui.main.details

import androidx.fragment.app.Fragment
import com.dimitare.bofindemo.di.keys.FragmentViewModelKey
import com.dimitare.bofindemo.di.scope.FragmentScope
import com.dimitare.bofindemo.ui.base.fragment.BaseViewModelFragmentModule
import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewModel
import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [BaseViewModelFragmentModule::class])
abstract class DetailsFragmentModule: BaseViewModel() {
    @Binds
    @FragmentScope
    abstract fun bindFragment(fragment: DetailsFragment): Fragment

    @Binds
    @IntoMap
    @FragmentViewModelKey(DetailsFragmentViewModel::class)
    @FragmentScope
    abstract fun bindViewModel(viewModel: DetailsFragmentViewModel): BaseViewFragmentViewModel
}