package com.dimitare.bofindemo.domain

import com.dimitare.bofindemo.data.helper.UserHelper
import com.dimitare.bofindemo.data.model.LoggedInUser
import com.dimitare.bofindemo.data.services.DemoRepository
import com.dimitare.bofindemo.ui.common.Gender
import io.reactivex.Flowable
import javax.inject.Inject

open class GetLoginUseCase @Inject constructor(
    private val mDemoRepository: DemoRepository
) {
    @Inject
    lateinit var userHelper: UserHelper

    fun login(user: String, password: String): Flowable<LoggedInUser> {
        return mDemoRepository.login(user, password)
    }

    fun updateGender(gender: Int): Flowable<Gender> {
        userHelper.updateGenderSelection(gender)
        return Flowable.just(userHelper.getGenderSelection())
    }

    fun updateUserData(loggedInUser: LoggedInUser): Flowable<String> {
        userHelper.updateUserData(loggedInUser)
        return Flowable.just(loggedInUser.token)
    }
}