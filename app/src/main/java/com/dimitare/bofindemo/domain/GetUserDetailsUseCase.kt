package com.dimitare.bofindemo.domain

import com.dimitare.bofindemo.data.model.User
import com.dimitare.bofindemo.data.services.DemoRepository
import io.reactivex.Single
import javax.inject.Inject

class GetUserDetailsUseCase @Inject constructor(private val mDemoRepository: DemoRepository){
    operator fun invoke(userId: Int): Single<User> {
        return mDemoRepository.getUserDetail(userId)
    }
}