package com.dimitare.bofindemo.domain

import com.dimitare.bofindemo.data.model.User
import com.dimitare.bofindemo.data.services.DemoRepository
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

class GetUsersListUseCase @Inject constructor(private val mDemoRepository: DemoRepository){
    operator fun invoke(page: Int, token: String, filerValue: String): Single<MutableList<User>> {
        return mDemoRepository.getUsers(page, token, filerValue)
    }
}