package com.dimitare.bofindemo.ext

import androidx.navigation.NavController
import com.dimitare.bofindemo.ui.common.navigation.NavigationRequest

fun NavController.navigate(
    request: NavigationRequest
) {
    when (request) {
        is NavigationRequest.Push -> navigate(
            request.destinationId ?: 0,
            request.args,
            request.options,
            request.extras
        )
        is NavigationRequest.Pop -> {
            if (request.destinationId != null && (request.destinationId > 0))
                popBackStack(request.destinationId, request.inclusive)
            else
                popBackStack()
        }
    }
}