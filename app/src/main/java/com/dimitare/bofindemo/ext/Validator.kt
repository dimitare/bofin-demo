package com.dimitare.bofindemo.ext

fun <T> T.validate(validator: Validator<T>): Boolean = validator.validate(this)

interface Validator<in T> {
    fun validate(any: T?): Boolean
}

private class UserValidator : Validator<CharSequence> {
    override fun validate(any: CharSequence?): Boolean = !any.isNullOrBlank() && any.length >= 4
}

val USER_VALIDATOR: Validator<CharSequence> = UserValidator()

private class PasswordValidator : Validator<CharSequence> {
    override fun validate(any: CharSequence?): Boolean = !any.isNullOrBlank() && any.length >= 4
}

val PASSWORD_VALIDATOR: Validator<CharSequence> = PasswordValidator()

