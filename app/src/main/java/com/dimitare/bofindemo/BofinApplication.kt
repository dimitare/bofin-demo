package com.dimitare.bofindemo

import android.content.Context
import androidx.multidex.MultiDex
import com.dimitare.bofindemo.di.DaggerAppComponent
import dagger.android.DaggerApplication

class BofinApplication: DaggerApplication(){
    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(newBase)
        MultiDex.install(this)
    }

    private val applicationInjector = DaggerAppComponent.builder()
        .application(this)
        .build()

    override fun applicationInjector() = applicationInjector
}