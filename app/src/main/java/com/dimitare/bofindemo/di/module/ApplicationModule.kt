package com.dimitare.bofindemo.di.module

import android.app.Application
import android.content.Context
import com.dimitare.bofindemo.BofinApplication
import com.dimitare.bofindemo.di.qualifier.ApplicationContext
import com.dimitare.bofindemo.data.services.DemoRemoteRepositoryModule
import com.dimitare.bofindemo.data.services.RetrofitModule
import com.dimitare.bofindemo.ui.FragmentBuilderModule
import dagger.Binds
import dagger.Module
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Module(
    includes = [AndroidSupportInjectionModule::class,
        FragmentBuilderModule::class,
        RetrofitModule::class,
        DemoRemoteRepositoryModule::class,
        SystemServiceModule::class]
)
abstract class ApplicationModule {
    @Binds
    @Singleton
    abstract fun bindApplication(application: BofinApplication): Application

    @Binds
    @Singleton
    @ApplicationContext
    abstract fun bindApplicationContext(application: Application): Context
}