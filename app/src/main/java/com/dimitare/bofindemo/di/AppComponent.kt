package com.dimitare.bofindemo.di

import com.dimitare.bofindemo.BofinApplication
import com.dimitare.bofindemo.di.module.ApplicationModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by @Dimitare on 06/13/20.
 */
@Singleton
@Component(modules = [ApplicationModule::class])
interface AppComponent : AndroidInjector<BofinApplication> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: BofinApplication): Builder

        fun build(): AppComponent
    }
}