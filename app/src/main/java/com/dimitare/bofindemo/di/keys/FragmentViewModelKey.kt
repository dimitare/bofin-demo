package com.dimitare.bofindemo.di.keys

import com.dimitare.bofindemo.ui.base.viewmodel.BaseViewFragmentViewModel
import dagger.MapKey
import kotlin.reflect.KClass

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
@MustBeDocumented
annotation class FragmentViewModelKey(val value: KClass<out BaseViewFragmentViewModel>)