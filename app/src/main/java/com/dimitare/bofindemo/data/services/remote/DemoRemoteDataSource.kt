package com.dimitare.bofindemo.data.services.remote

import com.dimitare.bofindemo.data.model.Token
import com.dimitare.bofindemo.data.services.DemoDataSource
import com.dimitare.bofindemo.data.model.User
import io.reactivex.Flowable

class DemoRemoteDataSource(
    private val mDemoService: DemoServiceApi
) : DemoDataSource {
    override fun login(loginRequest: LoginRequest): Flowable<Token> {
        return mDemoService.login(loginRequest)
    }

    override fun getUsers(authorization: String): Flowable<List<User>> {
        return mDemoService.getUsers(authorization)
    }
}