package com.dimitare.bofindemo.data.model

data class LoginError(val error: String? = null)