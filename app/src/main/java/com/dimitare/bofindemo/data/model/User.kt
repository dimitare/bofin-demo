package com.dimitare.bofindemo.data.model

import com.dimitare.bofindemo.ui.common.Gender
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class User @JsonCreator constructor(
    @JsonProperty("id") val id: Int,
    @JsonProperty("firstName") val firstName: String? = null,
    @JsonProperty("lastName") val lastName: String? = null,
    @JsonProperty("locationLongitude") val locationLongitude: Double? = 0.0,
    @JsonProperty("locationLatitude") val locationLatitude: Double? = 0.0,
    @JsonProperty("email") val email: String? = null,
    @JsonProperty("gender") val gender: Gender = Gender.Male,
    @JsonProperty("smallAvatar") val smallAvatar: String? = null,
    @JsonProperty("bigAvatar") val bigAvatar: String? = null
)