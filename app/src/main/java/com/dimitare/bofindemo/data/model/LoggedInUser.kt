package com.dimitare.bofindemo.data.model

data class LoggedInUser(
    val user: String,
    val token: String
)