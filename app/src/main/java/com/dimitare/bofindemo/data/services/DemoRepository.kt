package com.dimitare.bofindemo.data.services

import com.dimitare.bofindemo.data.model.LoggedInUser
import com.dimitare.bofindemo.data.model.User
import io.reactivex.Flowable
import io.reactivex.Single

interface DemoRepository {
    fun login(user: String, password: String): Flowable<LoggedInUser>
    fun getUsers(page: Int, token: String, filerValue: String): Single<MutableList<User>>
    fun getUserDetail(userId: Int): Single<User>
}