package com.dimitare.bofindemo.data.services

import com.dimitare.bofindemo.data.model.Token
import com.dimitare.bofindemo.data.model.User
import com.dimitare.bofindemo.data.services.remote.LoginRequest
import io.reactivex.Flowable

interface DemoDataSource {
    fun login(loginRequest: LoginRequest): Flowable<Token>
    fun getUsers(authorization: String): Flowable<List<User>>
}