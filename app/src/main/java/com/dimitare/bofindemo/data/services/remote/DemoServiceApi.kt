package com.dimitare.bofindemo.data.services.remote

import com.dimitare.bofindemo.BuildConfig
import com.dimitare.bofindemo.data.model.Token
import com.dimitare.bofindemo.data.model.User
import io.reactivex.Flowable
import retrofit2.http.*

interface DemoServiceApi {
    @Headers("Accept: application/json")
    @POST(BuildConfig.BOFIN_LOGIN_PATH)
    fun login(@Body loginRequest: LoginRequest): Flowable<Token>

    @GET(BuildConfig.BOFIN_GET_USERS_PATH)
    fun getUsers(@Header("Authorization") authorization: String): Flowable<List<User>>
}