package com.dimitare.bofindemo.data.helper

import com.dimitare.bofindemo.ui.common.Gender
import com.f2prateek.rx.preferences2.RxSharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SharedPreferencesHelper @Inject constructor(
    private val rxSharedPreferences: RxSharedPreferences
) {
    companion object {
        private const val ACCOUNT_NAME = "om.dimitare.bofindemo.data.helper.ACCOUNT_NAME"
        private const val ACCOUNT_TOKEN = "com.dimitare.bofindemo.data.helper.ACCOUNT_TOKEN"
        private const val GENDER_SELECTION = "com.dimitare.bofindemo.data.helper.GENDER_SELECTION"
    }

    val accountName by lazy { rxSharedPreferences.getString(ACCOUNT_NAME) }
    val accountToken by lazy { rxSharedPreferences.getString(ACCOUNT_TOKEN) }
    val gender by lazy { rxSharedPreferences.getInteger(GENDER_SELECTION, Gender.Male.ordinal) }
}