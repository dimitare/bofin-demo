package com.dimitare.bofindemo.data.services.remote

data class LoginRequest(val user: String, val password: String)