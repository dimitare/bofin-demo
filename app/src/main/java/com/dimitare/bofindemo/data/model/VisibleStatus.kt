package com.dimitare.bofindemo.data.model

import android.view.View

enum class VisibleStatus(val status: Int){
    VISIBLE(View.VISIBLE),
    INVISIBLE(View.INVISIBLE),
    GONE(View.GONE)
}