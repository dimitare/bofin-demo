package com.dimitare.bofindemo.data.helper

import android.accounts.AccountManager
import com.dimitare.bofindemo.data.model.LoggedInUser
import com.dimitare.bofindemo.data.model.Token
import com.dimitare.bofindemo.ui.common.Gender
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject
import javax.inject.Inject

open class UserHelper @Inject constructor(
    private val sharedPreferencesHelper: SharedPreferencesHelper
) {
    private val _statusObservable: BehaviorSubject<Boolean> = BehaviorSubject.create()
    val statusObservable: Observable<Boolean> = _statusObservable
    val token: String?
        get() {
            return sharedPreferencesHelper.accountToken.get()
        }

    fun isConnected() = !token.isNullOrBlank()

    fun disconnect() {
        sharedPreferencesHelper.accountToken.delete()

        if (_statusObservable.value != false) {
            _statusObservable.onNext(false)
        }
    }

    fun updateLogin() {
        val connected = isConnected()
        if (_statusObservable.value != connected) {
            _statusObservable.onNext(connected)
        }
    }

    fun updateUserData(loggedInUser: LoggedInUser): String{
        sharedPreferencesHelper.accountName.set(loggedInUser.user)
        sharedPreferencesHelper.accountToken.set(loggedInUser.token)
        return loggedInUser.token
    }

    fun getGenderSelection(): Gender {
        when(sharedPreferencesHelper.gender.get()){
            Gender.Female.ordinal -> return Gender.Female
        }
        return Gender.Male
    }

    fun updateGenderSelection(gender: Int) {
        sharedPreferencesHelper.gender.set(gender)
    }
}