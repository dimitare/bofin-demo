package com.dimitare.bofindemo.data.model

data class Token(val token: String)