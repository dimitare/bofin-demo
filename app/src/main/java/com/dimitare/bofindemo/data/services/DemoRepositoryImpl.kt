package com.dimitare.bofindemo.data.services

import android.util.Log
import com.dimitare.bofindemo.data.model.LoggedInUser
import com.dimitare.bofindemo.data.model.Token
import com.dimitare.bofindemo.data.model.User
import com.dimitare.bofindemo.data.services.remote.LoginRequest
import com.google.gson.GsonBuilder
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiConsumer
import org.json.JSONObject
import retrofit2.HttpException
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DemoRepositoryImpl @Inject constructor(
    private val mDemoSource: DemoDataSource
) : DemoRepository {

    private var _users: List<User>? = null
        private set

    override fun login(user: String, password: String): Flowable<LoggedInUser> {
        return mDemoSource.login(LoginRequest(user, password))
            .flatMap {
                Flowable.just(LoggedInUser(user, it.token))
            }
    }

    override fun getUsers(page: Int, token: String, filerValue: String): Single<MutableList<User>> {
        _users = null
        return mDemoSource.getUsers(token)
            .flatMapIterable { it }
            .filter {it -> it.gender.name == filerValue }
            .collect(
                { mutableListOf() },
                { list: MutableList<User>, user: User ->
                    list.add(user)
                })
            .flatMap {
                _users = it
                Single.just(it)
            }
    }

    override fun getUserDetail(userId: Int): Single<User> {
        for (index in 0 until (_users?.size ?: 0)) {
            val user = _users?.get(index)
            if( user?.id == userId ){
                return Single.just(user)
            }
        }
        throw IllegalArgumentException("User item with id: $userId not found")
    }
}